package model;

import java.util.Date;

public class Patient extends Personne{

	private String gs; 
	
	private int id;
	
	public String getGs() {
		return gs;
	}

	public void setGs(String gs) {
		this.gs = gs;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return this.nom;
	}
	
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public String getPrenom() {
		return prenom;
	}
	
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	
	public void setNaissance(Date date) {
		this.dateNaissance = date;
	}
	
	public Date getNaissance() {
		return dateNaissance;
	}

	
	public void setSexe(String sexe) {
		this.sexe = sexe;
	}
	
	public String getSexe() {
		return sexe;
	}
	
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	
	public String getTelephone() {
		return telephone;
	}

	public void setMatricule(String matricule) {
		this.matricule = matricule;
		
	}
	
	public String getMatricule() {
		return this.matricule;
	}
}
