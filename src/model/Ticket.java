package model;

import model.Patient;
import model.Personnel;

public class Ticket {

	private int id;
	private Specialite specialite;
	private Patient patient;
	private Personnel caissier;
	private String lien;
	
	public String getLien() {
		return lien;
	}
	public void setLien(String lien) {
		this.lien = lien;
	}
	public Personnel getCaissier() {
		return caissier;
	}
	public void setCaissier(Personnel caissier) {
		this.caissier = caissier;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Specialite getSpecialite() {
		return specialite;
	}
	public void setSpecialite(Specialite specialite) {
		this.specialite = specialite;
	}
	public Patient getPatient() {
		return patient;
	}
	public void setPatient(Patient patient) {
		this.patient = patient;
	}
}
