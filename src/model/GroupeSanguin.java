package model;

public enum GroupeSanguin {

	A,
	APOSITIF,
	ANEGATIF,
	O,
	OPOSITIF,
	ONEGATIF,
	B,
	BPOSITIF,
	BNEGATIF,
	ABPOSITIF,
	OBNEGATIF,
		
}
