package model;

import java.util.Date;

public abstract class Personne {

	protected String nom;
	protected String prenom;
	protected String type;
	protected String matricule;
	protected Date dateNaissance;
	protected String sexe;
	protected String telephone;
}
