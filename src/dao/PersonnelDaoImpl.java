package dao;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.itextpdf.io.IOException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import model.Specialite;
import model.Patient;
import model.Personne;
import model.Personnel;
import model.Ticket;

public class PersonnelDaoImpl implements PersonnelDao {
	
	private DAOFactory daoFactory;
	
	private static final String SQL_INSERT = "INSERT INTO personnel (nom, prenom, matricule, telephone, email, sexe, domaine, type, date_naissance, password) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	private static final String SQL_UPDATE = "UPDATE personnel SET nom=?, prenom=?, matricule=?, telephone=?, sexe=?, domaine=?, type=?, date_naissance=? where id=?";
	private static final String SQL_SELECT_PAR_EMAIL = "SELECT id, email, nom, prenom, telephone, matricule, domaine, type, date_naissance FROM personnel";
	private static final String SQL_SELECT_PAR_ID = "SELECT id, email, nom, prenom, telephone, matricule, domaine, type, date_naissance FROM personnel where id = ?";
	public static final String SALT = "consultation";
	private static final String SQL_AUTH = "SELECT id, id_admin, password, email, nom, prenom, telephone, matricule, domaine, type, date_naissance FROM personnel where email = ?";
	private static final String SQL_SELECT_RECHERCHE_PATIENT = "SELECT * FROM patient where telephone=?";
	private static final String SQL_INSERT_PATIENT = "INSERT INTO patient (nom, prenom, groupe_sanguin, matricule, telephone, sexe, date_naissance) VALUES (?, ?, ?, ?, ?, ?, ?)";
	private static final String SQL_INSERT_TICKET = "INSERT INTO ticket (specialite, id_patient, id_caissier, lien, date) VALUES (?, ?, ?, ?, NOW())";
	private static final String SQL_SELECT_TICKETS = "SELECT * FROM ticket";
	private static final String SQL_SELECT_PATIENT_PAR_ID = "SELECT id, nom, prenom, groupe_sanguin, telephone, matricule, date_naissance FROM patient where id = ?";
	private static final String SQL_SELECT_CAISSE = "SELECT * FROM ticket WHERE date=DATE( NOW() ) AND id_caissier=?";
	private static final String SQL_SUM_TARIF = "SELECT SUM(tarif) total FROM ticket join specialite on ticket.specialite=specialite.id WHERE date=DATE( NOW() ) AND id_caissier=?";
	private static final String SQL_SELECT_SEPECIALITE = "SELECT * FROM specialite WHERE id=?";

	
	PersonnelDaoImpl( DAOFactory daoFactory ) {
        this.daoFactory = daoFactory;
    }

	@Override
	public void creer(Personnel personnel) throws DAOException {
	    Connection connexion = null;
	    PreparedStatement preparedStatement = null;
	    ResultSet valeursAutoGenerees = null;

	    try {
	        connexion = daoFactory.getConnection();
	        preparedStatement = daoFactory.initialisationRequetePreparee( connexion, SQL_INSERT, true, personnel.getNom(), personnel.getPrenom(), personnel.getMatricule(),
	        personnel.getTelephone(), personnel.getEmail(), personnel.getSexe(), personnel.getDomaine(), personnel.getType(), personnel.getNaissance(), personnel.getPassword());
	        int statut = preparedStatement.executeUpdate();
	        if ( statut == 0 ) {
	            throw new DAOException( "�chec de la cr�ation du personnel, aucune ligne ajout�e dans la table." );
	        }

	        valeursAutoGenerees = preparedStatement.getGeneratedKeys();
	        if ( valeursAutoGenerees.next() ) {
	        } else {
	            throw new DAOException( "�chec de la cr�ation du personnel en base, aucun ID auto-g�n�r� retourn�." );
	        }
	    } catch ( SQLException e ) {
	        throw new DAOException( e );
	    } finally {
	        if ( connexion != null ) {
	            try {
	                connexion.close();
	            } catch ( SQLException e ) {
	                System.out.println( "�chec de la fermeture de la connexion : " + e.getMessage() );
	            }
	        }
	    }
		
	}
	
	
	@Override
	public void modifierPersonnel(Personnel personnel) throws DAOException {
	    Connection connexion = null;
	    PreparedStatement preparedStatement = null;
	    ResultSet valeursAutoGenerees = null;

	    try {
	        connexion = daoFactory.getConnection();
	        System.out.println("default id "+personnel.getId());
	        preparedStatement = daoFactory.initialisationRequetePreparee( connexion, SQL_UPDATE, false, personnel.getNom(), personnel.getPrenom(), personnel.getMatricule(),
	        personnel.getTelephone(), personnel.getSexe(), personnel.getDomaine(), personnel.getType(), personnel.getNaissance(), personnel.getId());
	        int statut = preparedStatement.executeUpdate();
	        if ( statut == 0 ) {
	            throw new DAOException( "�chec de la mise � jour du personnel" );
	        }

	    } catch ( SQLException e ) {
	        throw new DAOException( e );
	    } finally {
	        if ( connexion != null ) {
	            try {
	                connexion.close();
	            } catch ( SQLException e ) {
	                System.out.println( "�chec de la fermeture de la connexion : " + e.getMessage() );
	            }
	        }
	    }
		
	}
	
	@Override
	public List<Personnel>  getAllPersonnel() throws DAOException {
	    Connection connexion = null;
	    PreparedStatement preparedStatement = null;
	    ResultSet resultSet = null;
	    List<Personnel> personnel = new ArrayList();

	    try {
	        /* R�cup�ration d'une connexion depuis la Factory */
	        connexion = daoFactory.getConnection();
	        preparedStatement = daoFactory.initialisationRequetePreparee( connexion, SQL_SELECT_PAR_EMAIL, false );
	        resultSet = preparedStatement.executeQuery();
	        /* Parcours de la ligne de donn�es de l'�ventuel ResulSet retourn� */
	        while( resultSet.next() ) {
	            personnel.add(map( resultSet ));
	        }

	    } catch ( SQLException e ) {
	        throw new DAOException( e );
	    } finally {
	        fermeturesSilencieuses( resultSet, preparedStatement, connexion );
	    }

	    return personnel;
	}
	
	
	@Override
	public List<Ticket>  mesTickets() throws DAOException{
	    Connection connexion = null;
	    PreparedStatement preparedStatement = null;
	    ResultSet resultSet = null;
	    List<Ticket> tickets = new ArrayList();

	    try {
	        /* R�cup�ration d'une connexion depuis la Factory */
	        connexion = daoFactory.getConnection();
	        preparedStatement = daoFactory.initialisationRequetePreparee( connexion, SQL_SELECT_TICKETS, false );
	        resultSet = preparedStatement.executeQuery();
	        /* Parcours de la ligne de donn�es de l'�ventuel ResulSet retourn� */
	        while( resultSet.next() ) {
	        	Personnel caissier = rechercherPersonnel(resultSet.getInt("id_caissier"));
	        	Patient patient = rechercherPatient(resultSet.getInt("id_patient"));
	        	Specialite specialite = getSpecialiteId(resultSet.getInt("specialite"));
	        	tickets.add(mapTickets(resultSet, patient, caissier, specialite));
	        }

	    } catch ( SQLException e ) {
	        throw new DAOException( e );
	    } finally {
	        fermeturesSilencieuses( resultSet, preparedStatement, connexion );
	    }

	    return tickets;
	}
	
	@Override
	public void vendreTicket( Ticket ticket ) throws DAOException{
	    Connection connexion = null;
	    PreparedStatement preparedStatement = null;
	    ResultSet valeursAutoGenerees = null;

	    try {
	    	
	    	System.out.println("sepe "+ticket.getSpecialite());
	    	System.out.println("pat "+ticket.getPatient().getId());
	    	System.out.println("caiss"+ticket.getCaissier().getId());
	    	
	    	
	    	String lien = "doc"+getRandomNumber();
	        connexion = daoFactory.getConnection();
	        preparedStatement = daoFactory.initialisationRequetePreparee( connexion, SQL_INSERT_TICKET, true, 
	        ticket.getSpecialite().getId(), ticket.getPatient().getId(), ticket.getCaissier().getId(), lien);
	        
	        
	        int statut = preparedStatement.executeUpdate();
	        if ( statut == 0 ) {
	            throw new DAOException( "�chec de la cr�ation du patient, aucune ligne ajout�e dans la table." );
	        }

	        valeursAutoGenerees = preparedStatement.getGeneratedKeys();
	        if ( valeursAutoGenerees.next() ) {
			    try {
				      OutputStream file = new FileOutputStream(new File("C:\\Users\\babs\\eclipse-workspace\\Consultation\\WebContent\\tickets\\"+lien+".pdf"));
				      Document document = new Document();
				      PdfWriter.getInstance(document, file);
				      document.open();
				      document.add(new Paragraph("Bonjour et bienvenu sur JAVA EE Hospital"));
				      document.add(new Paragraph("ID ticket : "+valeursAutoGenerees.getInt(1)));
				      document.add(new Paragraph("Nom : "+ticket.getPatient().getNom()+" "+ticket.getPatient().getPrenom()));
				      document.add(new Paragraph("Sp�cialt� : "+ticket.getSpecialite()));
				      document.add(new Paragraph(new Date().toString()));
				      document.close();
				      file.close();
				    } catch (Exception e) {
				      e.printStackTrace();
				    }
	        } else {
	            throw new DAOException( "�chec de la cr�ation du patient en base, aucun ID auto-g�n�r� retourn�." );
	        }
	    } catch ( SQLException e ) {
	        throw new DAOException( e );
	    } finally {
	        if ( connexion != null ) {
	            try {
	                connexion.close();
	            } catch ( SQLException e ) {
	                System.out.println( "�chec de la fermeture de la connexion : " + e.getMessage() );
	            }
	        }
	    }
	    
	}
	
	@Override
	public Personnel login(Personnel user) throws DAOException {
	    Connection connexion = null;
	    PreparedStatement preparedStatement = null;
	    ResultSet resultSet = null;
	    Personnel personnel = new Personnel();

	    try {
	        /* R�cup�ration d'une connexion depuis la Factory */
	        connexion = daoFactory.getConnection();
	        preparedStatement = daoFactory.initialisationRequetePreparee( connexion, SQL_AUTH, false, user.getEmail() );
	        resultSet = preparedStatement.executeQuery();
	        
			String saltedPassword = SALT + user.getPassword();
			String hashedPassword = generateHash(saltedPassword);

	        if( resultSet.next() ) {
	        	
	        	if(resultSet.getString("password").equals(hashedPassword)) {

	        		personnel = map( resultSet );
	        		personnel.setLogin(true);
	        	}else{

	        		personnel.setLogin(false);
	        		personnel.setEmail(user.getEmail());
	        	}
	            
	        }else {
	        	personnel = null;
	        }

	    } catch ( SQLException e ) {
	        throw new DAOException( e );
	    } finally {
	        fermeturesSilencieuses( resultSet, preparedStatement, connexion );
	    }

	    return personnel;
		
	}
	
	@Override
	public Patient getPatient(String tel) throws DAOException {
	    Connection connexion = null;
	    PreparedStatement preparedStatement = null;
	    ResultSet resultSet = null;
	    Patient patient = new Patient();

	    try {
	        /* R�cup�ration d'une connexion depuis la Factory */
	        connexion = daoFactory.getConnection();
	        preparedStatement = daoFactory.initialisationRequetePreparee( connexion, SQL_SELECT_RECHERCHE_PATIENT, false, tel);
	        resultSet = preparedStatement.executeQuery();

	        while( resultSet.next() ) {
	        	patient = mapPatient( resultSet );
	        }

	    } catch ( SQLException e ) {
	        throw new DAOException( e );
	    } finally {
	        fermeturesSilencieuses( resultSet, preparedStatement, connexion );
	    }

	    return patient;
	}
	
	
	@Override
	public int ajouterPatient( Patient patient ) throws DAOException {
	    Connection connexion = null;
	    PreparedStatement preparedStatement = null;
	    ResultSet valeursAutoGenerees = null;
	    int resultat = -1;

	    try {
	        connexion = daoFactory.getConnection();
	        preparedStatement = daoFactory.initialisationRequetePreparee( connexion, SQL_INSERT_PATIENT, true, patient.getNom(), patient.getPrenom(), patient.getGs(), patient.getMatricule(),
	        patient.getTelephone(), patient.getSexe(), patient.getNaissance());
	        int statut = preparedStatement.executeUpdate();
	        if ( statut == 0 ) {
	            throw new DAOException( "�chec de la cr�ation du patient, aucune ligne ajout�e dans la table." );
	        }

	        valeursAutoGenerees = preparedStatement.getGeneratedKeys();
	        if ( valeursAutoGenerees.next() ) {
	        	
	        	resultat = valeursAutoGenerees.getInt(1);
	        	
	        } else {
	            throw new DAOException( "�chec de la cr�ation du patient en base, aucun ID auto-g�n�r� retourn�." );
	        }
	    } catch ( SQLException e ) {
	        throw new DAOException( e );
	    } finally {
	        if ( connexion != null ) {
	            try {
	                connexion.close();
	            } catch ( SQLException e ) {
	                System.out.println( "�chec de la fermeture de la connexion : " + e.getMessage() );
	            }
	        }
	    }
	    
	    return resultat;
	}
	
	@Override
	public void ajouterTicket( Ticket ticket ) throws DAOException{
	    Connection connexion = null;
	    PreparedStatement preparedStatement = null;
	    ResultSet valeursAutoGenerees = null;

	    try {
	    	
	    	System.out.println("sepe "+ticket.getSpecialite());
	    	System.out.println("pat "+ticket.getPatient().getId());
	    	System.out.println("caiss"+ticket.getCaissier().getId());
	    	
	        connexion = daoFactory.getConnection();
	        preparedStatement = daoFactory.initialisationRequetePreparee( connexion, SQL_INSERT_TICKET, true, 
	        ticket.getSpecialite(), ticket.getPatient().getId(), ticket.getCaissier().getId());
	        int statut = preparedStatement.executeUpdate();
	        if ( statut == 0 ) {
	            throw new DAOException( "�chec de la cr�ation du patient, aucune ligne ajout�e dans la table." );
	        }

	        valeursAutoGenerees = preparedStatement.getGeneratedKeys();
	        if ( valeursAutoGenerees.next() ) {
	        } else {
	            throw new DAOException( "�chec de la cr�ation du patient en base, aucun ID auto-g�n�r� retourn�." );
	        }
	    } catch ( SQLException e ) {
	        throw new DAOException( e );
	    } finally {
	        if ( connexion != null ) {
	            try {
	                connexion.close();
	            } catch ( SQLException e ) {
	                System.out.println( "�chec de la fermeture de la connexion : " + e.getMessage() );
	            }
	        }
	    }
	}
	
	@Override
	public Personnel rechercherPersonnel(int id) throws DAOException {
	    Connection connexion = null;
	    PreparedStatement preparedStatement = null;
	    ResultSet resultSet = null;
	    Personnel personnel = new Personnel();

	    try {
	        /* R�cup�ration d'une connexion depuis la Factory */
	        connexion = daoFactory.getConnection();
	        preparedStatement = daoFactory.initialisationRequetePreparee( connexion, SQL_SELECT_PAR_ID, false, id );
	        resultSet = preparedStatement.executeQuery();

	        while( resultSet.next() ) {
	            personnel = map( resultSet );
	        }

	    } catch ( SQLException e ) {
	        throw new DAOException( e );
	    } finally {
	        fermeturesSilencieuses( resultSet, preparedStatement, connexion );
	    }

	    return personnel;
	}

	
	@Override
	public Patient rechercherPatient(int id) throws DAOException {
	    Connection connexion = null;
	    PreparedStatement preparedStatement = null;
	    ResultSet resultSet = null;
	    Patient patient = new Patient();

	    try {
	        /* R�cup�ration d'une connexion depuis la Factory */
	        connexion = daoFactory.getConnection();
	        preparedStatement = daoFactory.initialisationRequetePreparee( connexion, SQL_SELECT_PATIENT_PAR_ID, false, id );
	        resultSet = preparedStatement.executeQuery();

	        while( resultSet.next() ) {
	        	patient = mapPatient( resultSet );
	        }

	    } catch ( SQLException e ) {
	        throw new DAOException( e );
	    } finally {
	        fermeturesSilencieuses( resultSet, preparedStatement, connexion );
	    }

	    return patient;
	}
	/* Fermeture silencieuse du resultset */
	public static void fermetureSilencieuse( ResultSet resultSet ) {
	    if ( resultSet != null ) {
	        try {
	            resultSet.close();
	        } catch ( SQLException e ) {
	            System.out.println( "�chec de la fermeture du ResultSet : " + e.getMessage() );
	        }
	    }
	}

	/* Fermeture silencieuse du statement */
	public static void fermetureSilencieuse( Statement statement ) {
	    if ( statement != null ) {
	        try {
	            statement.close();
	        } catch ( SQLException e ) {
	            System.out.println( "�chec de la fermeture du Statement : " + e.getMessage() );
	        }
	    }
	}
	
	@Override
	public List<Ticket>  FaireLaCaisse(HttpServletRequest  request) throws DAOException{
	    Connection connexion = null;
	    PreparedStatement preparedStatement = null;
	    ResultSet resultSet = null;
	    List<Ticket> tickets = new ArrayList();

	    try {
	        /* R�cup�ration d'une connexion depuis la Factory */
	        HttpSession session = request.getSession();
	        
	        Personnel caissierOline = (Personnel) session.getAttribute("caissier");	
	        
	        connexion = daoFactory.getConnection();
	        preparedStatement = daoFactory.initialisationRequetePreparee( connexion, SQL_SELECT_CAISSE, false, caissierOline.getId());
	        resultSet = preparedStatement.executeQuery();
	        /* Parcours de la ligne de donn�es de l'�ventuel ResulSet retourn� */
	        

		    
	        while( resultSet.next() ) {
	        	Personnel caissier = rechercherPersonnel(resultSet.getInt("id_caissier"));
	        	Specialite specialite = getSpecialiteId(resultSet.getInt("specialite"));
	        	Patient patient = rechercherPatient(resultSet.getInt("id_patient"));
	        	tickets.add(mapTickets(resultSet, patient, caissier, specialite));
	        }
	        
		    try {
			      String DEST = "C:\\Users\\babs\\eclipse-workspace\\Consultation\\WebContent\\tickets\\total.pdf";
			      createPdf(DEST, tickets, caissierOline.getId());
			    } catch (Exception e) {
			      e.printStackTrace();
			    }

	    } catch ( SQLException e ) {
	        throw new DAOException( e );
	    } finally {
	        fermeturesSilencieuses( resultSet, preparedStatement, connexion );
	    }
	    
	    return tickets;
	}
	
	public Specialite getSpecialiteId(int id) throws DAOException {
	    Connection connexion = null;
	    PreparedStatement preparedStatement = null;
	    ResultSet resultSet = null;
	    Specialite specialite = new Specialite();

	    try {
	        /* R�cup�ration d'une connexion depuis la Factory */
	        connexion = daoFactory.getConnection();
	        preparedStatement = daoFactory.initialisationRequetePreparee( connexion, SQL_SELECT_SEPECIALITE, false, id );
	        resultSet = preparedStatement.executeQuery();

	        while( resultSet.next() ) {
	        	specialite = mapSpecialite( resultSet);
	        }

	    } catch ( SQLException e ) {
	        throw new DAOException( e );
	    } finally {
	        fermeturesSilencieuses( resultSet, preparedStatement, connexion );
	    }

	    return specialite;
	}
	
    private int getRandomNumber() {
    	String CHAR_LIST = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    	
        int randomInt = 0;
        Random randomGenerator = new Random();
        randomInt = randomGenerator.nextInt(CHAR_LIST.length());
        if (randomInt - 1 == -1) {
            return randomInt;
        } else {
            return randomInt - 1;
        }
    }
	

	/* Fermeture silencieuse de la connexion */
	public static void fermetureSilencieuse( Connection connexion ) {
	    if ( connexion != null ) {
	        try {
	            connexion.close();
	        } catch ( SQLException e ) {
	            System.out.println( "�chec de la fermeture de la connexion : " + e.getMessage() );
	        }
	    }
	}
	
    public void createPdf(String dest, List<Ticket> tickets, int id) throws IOException, DocumentException, FileNotFoundException {

        Document document = new Document();

        PdfWriter.getInstance(document, new FileOutputStream(dest));

        document.open();
        

        PdfPTable table = new PdfPTable(4);
        
        PdfPTable tableTotal = new PdfPTable(2);
        
        String[] colum = {"id ticket", "Patient", "Caissier", "Sp�cialit�"};

        String[] columTotal = {"Nombre de tickets vendus", "Montant total"};

        for(int aw = 0; aw < colum.length; aw++){

            table.addCell(colum[aw]);

        }
        
        
        for(int aw = 0; aw < tickets.size(); aw++){

        	String cellId = Integer.toString(tickets.get(aw).getId());
        	String cellPatient = tickets.get(aw).getPatient().getNom();
        	String cellCaissier = tickets.get(aw).getCaissier().getNom();
        	String cellSpecialite = tickets.get(aw).getSpecialite().getNom();
            table.addCell(cellId);
            table.addCell(cellPatient);
            table.addCell(cellCaissier);
            table.addCell(cellSpecialite);
            

        }

        for(int aw = 0; aw < columTotal.length; aw++){

        	tableTotal.addCell(columTotal[aw]);

        }
        
        int nbrTicket = tickets.size();

        int montantTotal = this.getTotal(id);
        

    	tableTotal.addCell(Integer.toString(nbrTicket));
    	tableTotal.addCell(Integer.toString(montantTotal));

        document.add(table);
        document.add(tableTotal);

        document.close();
    }
    
	@Override
	public int getTotal(int id) throws DAOException {
	    Connection connexion = null;
	    PreparedStatement preparedStatement = null;
	    ResultSet resultSet = null;
	    int total = 0;
	    
	    try {
	        /* R�cup�ration d'une connexion depuis la Factory */
	        connexion = daoFactory.getConnection();
	        preparedStatement = daoFactory.initialisationRequetePreparee( connexion, SQL_SUM_TARIF, false, id );
	        resultSet = preparedStatement.executeQuery();

	        while( resultSet.next() ) {
	        	total = resultSet.getInt("total");
	        }

	    } catch ( SQLException e ) {
	        throw new DAOException( e );
	    } finally {
	        fermeturesSilencieuses( resultSet, preparedStatement, connexion );
	    }

	    return total;
	}

	/* Fermetures silencieuses du statement et de la connexion */
	public static void fermeturesSilencieuses( Statement statement, Connection connexion ) {
	    fermetureSilencieuse( statement );
	    fermetureSilencieuse( connexion );
	}

	/* Fermetures silencieuses du resultset, du statement et de la connexion */
	public static void fermeturesSilencieuses( ResultSet resultSet, Statement statement, Connection connexion ) {
	    fermetureSilencieuse( resultSet );
	    fermetureSilencieuse( statement );
	    fermetureSilencieuse( connexion );
	}
	
	private static Personnel map( ResultSet resultSet ) throws SQLException {
	    Personnel personnel = new Personnel();
	    personnel.setNom( resultSet.getString( "nom" ) );
	    personnel.setId( resultSet.getInt( "id" ) );
	    personnel.setEmail( resultSet.getString( "email" ) );
	    personnel.setDomaine( resultSet.getString( "domaine" ) );
	    personnel.setType( resultSet.getString( "type" ) );
	    personnel.setNaissance( resultSet.getTimestamp( "date_naissance" ) );
	    personnel.setTelephone( resultSet.getString( "telephone" ) );
	    personnel.setMatricule( resultSet.getString( "matricule" ) );
	    personnel.setPrenom( resultSet.getString( "prenom" ) );
	    return personnel;
	}
	
	
	private static Patient mapPatient( ResultSet resultSet ) throws SQLException {
		Patient patient = new Patient();
		patient.setNom( resultSet.getString( "nom" ) );
		patient.setId( resultSet.getInt( "id" ) );
		patient.setNaissance( resultSet.getTimestamp( "date_naissance" ) );
		patient.setTelephone( resultSet.getString( "telephone" ) );
		patient.setMatricule( resultSet.getString( "matricule" ) );
		patient.setPrenom( resultSet.getString( "prenom" ) );
		patient.setGs( resultSet.getString( "groupe_sanguin" ) );
	    return patient;
	}
	
	
	private static Ticket mapTickets( ResultSet resultSet, Patient patient, Personnel caissier, Specialite specialite ) throws SQLException {
		Ticket ticket = new Ticket();
		ticket.setId(resultSet.getInt("id"));
		ticket.setPatient(patient);
		ticket.setCaissier(caissier);
		ticket.setSpecialite(specialite);
		ticket.setLien(resultSet.getString("lien"));
		
	    return ticket;
	}
	
	public static String generateHash(String input) {
		StringBuilder hash = new StringBuilder();

		try {
			MessageDigest sha = MessageDigest.getInstance("SHA-1");
			byte[] hashedBytes = sha.digest(input.getBytes());
			char[] digits = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
					'a', 'b', 'c', 'd', 'e', 'f' };
			for (int idx = 0; idx < hashedBytes.length; ++idx) {
				byte b = hashedBytes[idx];
				hash.append(digits[(b & 0xf0) >> 4]);
				hash.append(digits[b & 0x0f]);
			}
		} catch (NoSuchAlgorithmException e) {
			// handle error here.
		}

		return hash.toString();
	}
	
	private static Specialite mapSpecialite( ResultSet resultSet) throws SQLException {
		Specialite specialite = new Specialite();
		specialite.setNom( resultSet.getString( "nom" ) );
		specialite.setId( resultSet.getInt( "id" ) );
		specialite.setTarif( resultSet.getInt( "tarif" ) );
	    return specialite;
	}

}
