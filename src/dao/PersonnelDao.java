package dao;

import java.sql.ResultSet;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import model.Patient;
import model.Personne;
import model.Personnel;
import model.Ticket;

public interface PersonnelDao {

    void creer( Personnel personnel ) throws DAOException;
    
    int ajouterPatient( Patient patient ) throws DAOException;
    
    void ajouterTicket( Ticket ticket ) throws DAOException;
    
    List<Personnel>  getAllPersonnel() throws DAOException;
    
    List<Ticket>  mesTickets() throws DAOException;
    
    Personnel rechercherPersonnel(int id) throws DAOException;
    
    Patient rechercherPatient(int id) throws DAOException;
    
    void modifierPersonnel(Personnel personnel) throws DAOException;
    
    Personnel login(Personnel pers) throws DAOException;
    
    public Patient getPatient(String tel) throws DAOException;
    
    List<Ticket>  FaireLaCaisse(HttpServletRequest  request) throws DAOException;
    
    int getTotal(int id) throws DAOException;
    
    void vendreTicket( Ticket ticket ) throws DAOException;

}
