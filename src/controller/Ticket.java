package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOFactory;
import dao.PersonnelDao;
import form.TicketForm;
import model.Patient;

@WebServlet("/caissier/ajout/ticket")
public class Ticket extends HttpServlet{

	private PersonnelDao personnelDao;
	public static final String CONF_DAO_FACTORY = "daofactory"; 	
	
    public void init() throws ServletException {
        /* Récupération d'une instance de notre DAO Utilisateur */
        this.personnelDao = ( (DAOFactory) this.getServletContext().getAttribute( CONF_DAO_FACTORY ) ).getPersonnelDao();
        
        
    }
    
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		if(req.getParameter("telPatient") != null) {
			String tel = req.getParameter("telPatient");
			
			Patient patient = personnelDao.getPatient(tel);
			
			System.out.println(patient.getNom());
			
			req.setAttribute("patient", patient);
		}
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/template/ajoutTicket.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		TicketForm form = new TicketForm(personnelDao);
		
		form.addTicket(req);
		
		resp.sendRedirect("http://localhost:8080/hospital/caissier");
	}

}
