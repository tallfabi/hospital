package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Personnel;
import dao.DAOFactory;
import dao.PersonnelDao;
import form.InsertionPersonnelForm;

@WebServlet("/ajout/personnel")
public class AjoutPersonnel extends HttpServlet{

	public static final String CONF_DAO_FACTORY = "daofactory"; 	
	private PersonnelDao personnelDao;
	
    public void init() throws ServletException {
        /* Récupération d'une instance de notre DAO Utilisateur */
        this.personnelDao = ( (DAOFactory) this.getServletContext().getAttribute( CONF_DAO_FACTORY ) ).getPersonnelDao();
    }
    
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		this.getServletContext().getRequestDispatcher("/WEB-INF/template/ajoutp.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		InsertionPersonnelForm form = new InsertionPersonnelForm(personnelDao);
		
		Personnel personnel = form.ajouterPersonnel(req);
		
        req.setAttribute( "form", form );
        req.setAttribute( "personnel", personnel );
		
        resp.sendRedirect("http://localhost:8080/hospital/personnels");
	}

}
