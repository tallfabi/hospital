package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.DAOFactory;
import dao.PersonnelDao;
import form.LoginForm;
import model.Personnel;

@WebServlet("/login")
public class Login extends HttpServlet{

	public static final String CONF_DAO_FACTORY = "daofactory"; 	
	private PersonnelDao personnelDao;
	
    public void init() throws ServletException {
        /* Récupération d'une instance de notre DAO Utilisateur */
        this.personnelDao = ( (DAOFactory) this.getServletContext().getAttribute( CONF_DAO_FACTORY ) ).getPersonnelDao();
    }
    
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		this.getServletContext().getRequestDispatcher("/WEB-INF/template/login.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		String password = req.getParameter("password");
		
		LoginForm login = new LoginForm(personnelDao);
		
		Personnel personnel = login.login(req);
		
		if(personnel == null) {
			
			
			req.setAttribute("error", "E-mail ou mot de passe incorrect");
			req.setAttribute("email", req.getParameter("email"));
			
			this.getServletContext().getRequestDispatcher("/WEB-INF/template/login.jsp").forward(req, resp);
			
		}else {
			
				if(!personnel.isLogin()) {

					System.out.println("Login ou mot de passe incorrect !");
					req.setAttribute("error", "Login ou mot de passe incorrect ! ");
					req.setAttribute("email", req.getParameter("email"));
					
					this.getServletContext().getRequestDispatcher("/WEB-INF/template/login.jsp").forward(req, resp);
				}else if (personnel.getType().equals("CAISSIER")){

			        HttpSession session = req.getSession();
			        
			        session.setAttribute( "caissier", personnel);	
			        
					resp.sendRedirect("http://localhost:8080/hospital/caissier");

				}else {
					
			        HttpSession session = req.getSession();
			        
			        session.setAttribute( "admin", personnel);	
			        
					resp.sendRedirect("http://localhost:8080/hospital/personnels");
				}

			
		}
	}

	
}
