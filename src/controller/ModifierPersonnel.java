package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOFactory;
import dao.PersonnelDao;
import form.ModifierPersonnelForm;
import model.Personnel;

@WebServlet("/modifier/personnel")
public class ModifierPersonnel extends HttpServlet{

	public static final String CONF_DAO_FACTORY = "daofactory"; 	
	private PersonnelDao personnelDao;
	
    public void init() throws ServletException {
        /* Récupération d'une instance de notre DAO Utilisateur */
        this.personnelDao = ( (DAOFactory) this.getServletContext().getAttribute( CONF_DAO_FACTORY ) ).getPersonnelDao();
    }
    
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		int idPersonnel = Integer.parseInt(req.getParameter("idPersonnel"));
		
		Personnel personnel = personnelDao.rechercherPersonnel(idPersonnel);
        	
		req.setAttribute( "personnel", personnel );
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/template/modifierpersonnel.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		ModifierPersonnelForm form = new ModifierPersonnelForm(personnelDao);
		
		Personnel personnel = form.modifierPersonnel(req);
		
        req.setAttribute( "form", form );
        req.setAttribute( "personnel", personnel );
		
        resp.sendRedirect("http://localhost:8080/hospital/personnels");
	}

}
