package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Personnel;
import model.Ticket;
import dao.DAOFactory;
import dao.PersonnelDao;

@WebServlet("/caissier/fairelacaisse")
public class FaireLaCaisse extends HttpServlet{
	public static final String CONF_DAO_FACTORY = "daofactory"; 	
	private PersonnelDao personnelDao;
	
    public void init() throws ServletException {
        /* Récupération d'une instance de notre DAO Utilisateur */
        this.personnelDao = ( (DAOFactory) this.getServletContext().getAttribute( CONF_DAO_FACTORY ) ).getPersonnelDao();
    }
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		List<Ticket> tickets = personnelDao.FaireLaCaisse(req);
		
		System.out.println("tickets length "+tickets.size());

		req.setAttribute( "tickets", tickets );
		
		int size = tickets.size();
		
        HttpSession session = req.getSession();
        
        Personnel caissierOline = (Personnel) session.getAttribute("caissier");	
		
		int total = personnelDao.getTotal(caissierOline.getId());
		
		req.setAttribute( "nbrTicket", size );
		
		req.setAttribute( "total", total );
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/template/fairelacaisse.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		super.doPost(req, resp);
	}

}
