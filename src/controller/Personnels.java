package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.List;

import dao.DAOFactory;
import dao.PersonnelDao;

@WebServlet("/personnels")
public class Personnels extends HttpServlet{
	
	private PersonnelDao personnelDao;
	public static final String CONF_DAO_FACTORY = "daofactory"; 	
	
    public void init() throws ServletException {
        /* Récupération d'une instance de notre DAO Utilisateur */
        this.personnelDao = ( (DAOFactory) this.getServletContext().getAttribute( CONF_DAO_FACTORY ) ).getPersonnelDao();
    }

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		List<model.Personnel> personnel = personnelDao.getAllPersonnel();
		
        for (model.Personnel personnel2 : personnel) {
			System.out.println(personnel2.getNom());
		}
        
		req.setAttribute( "personnel", personnel );
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/template/home.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		super.doPost(req, resp);
	}

}
