package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOFactory;
import dao.PersonnelDao;

@WebServlet("/caissier")
public class Caissier extends HttpServlet{

	private PersonnelDao personnelDao;
	public static final String CONF_DAO_FACTORY = "daofactory"; 	
	
    public void init() throws ServletException {
        /* Récupération d'une instance de notre DAO Utilisateur */
        this.personnelDao = ( (DAOFactory) this.getServletContext().getAttribute( CONF_DAO_FACTORY ) ).getPersonnelDao();
    }
    
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		List<model.Ticket> tickets = personnelDao.mesTickets();
		
		System.out.println("tickets length "+tickets.size());

		req.setAttribute( "tickets", tickets );
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/template/caissier.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		super.doPost(req, resp);
	}

}
