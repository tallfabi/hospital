package form;

import javax.servlet.http.HttpServletRequest;

import model.Personnel;
import dao.PersonnelDao;

public class LoginForm {

    private static final String CHAMP_EMAIL    = "email";
    private static final String CHAMP_PASSWORD    = "password";
    
	private PersonnelDao personnelDao;
	

	public LoginForm(PersonnelDao auth) {
		this.personnelDao = auth;
	}
	
	public Personnel login(HttpServletRequest  request) {

	    String email = getValeurChamp( request, CHAMP_EMAIL );
	    String password = getValeurChamp( request, CHAMP_PASSWORD );
	    
	    Personnel personnel = new Personnel();
	    
        personnel.setEmail(email);
        personnel.setPassword(password);
                
	    return personnelDao.login(personnel);
	}
	
	private static String getValeurChamp( HttpServletRequest request, String nomChamp ) {
	    String valeur = request.getParameter( nomChamp );
	    if ( valeur == null || valeur.trim().length() == 0 ) {
	        return null;
	    } else {
	        return valeur.trim();
	    }
	}
}
