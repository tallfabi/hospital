package form;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import dao.DAOException;
import dao.PersonnelDao;
import model.Personnel;

public class ModifierPersonnelForm {
    private static final String CHAMP_EMAIL  = "email";
    private static final String CHAMP_NOM    = "nom";
    private static final String CHAMP_PRENOM    = "prenom";
    private static final String CHAMP_SEXE    = "sexe";
    private static final String CHAMP_TELEPHONE    = "telephone";
    private static final String CHAMP_DOMAINE    = "domaine";
    private static final String CHAMP_TYPE    = "type";
    private static final String CHAMP_NAISSANCE    = "naissance";
    
	private PersonnelDao personnelDao;
	
	private String resultat;
	private Map<String, String> erreurs = new HashMap<String, String>();

	public String getResultat() {
	    return resultat;
	}

	public Map<String, String> getErreurs() {
	    return erreurs;
	}
	

	public ModifierPersonnelForm(PersonnelDao personnelDao) {
		this.personnelDao = personnelDao;
	}
	
	public Personnel modifierPersonnel(HttpServletRequest  request) {

	    String email = getValeurChamp( request, CHAMP_EMAIL );
	    String nom = getValeurChamp( request, CHAMP_NOM );
	    String prenom = getValeurChamp( request, CHAMP_PRENOM );
	    String sexe = getValeurChamp( request, CHAMP_SEXE );	
	    String domaine = getValeurChamp( request, CHAMP_DOMAINE );	
	    String type = getValeurChamp( request, CHAMP_TYPE );	
	    String naissance = getValeurChamp( request, CHAMP_NAISSANCE );	
	    String telephone = getValeurChamp( request, CHAMP_TELEPHONE );	
	    
	    Personnel personnel = new Personnel();
	    
	    int idPersonnel = Integer.parseInt(request.getParameter("idPersonnel"));
	    
	    personnel.setId(idPersonnel);
	    
	    try {
	        validationEmail( email );
	    } catch ( Exception e ) {
	        setErreur( CHAMP_EMAIL, e.getMessage() );
	    }
	    personnel.setEmail( email );
	    
	    try {
	        validationNom( nom );
	    } catch ( Exception e ) {
	        setErreur( CHAMP_NOM, e.getMessage() );
	    }
	    personnel.setNom( nom );
	    
	    Random rand = new Random();
	    String uniqueID = String.format("%04d", rand.nextInt(10000));
	    personnel.setMatricule(uniqueID);
	    
	    DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
	    Date fd = new Date();
	    
		try {
			fd = formatter.parse(naissance);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	    java.sql.Date sqlDate = new java.sql.Date(fd.getTime());
	    

        personnel.setNaissance(sqlDate);
        
        personnel.setSexe(sexe);
        personnel.setTelephone(telephone);
        
        personnel.setDomaine(domaine);
        personnel.setType(type);
        
        personnel.setPrenom(prenom);
        
        try {

            if ( erreurs.isEmpty() ) {
            	
	            personnelDao.modifierPersonnel( personnel );
	            resultat = "Succ�s de l'inscription.";            		

            } else {
                resultat = "�chec de l'inscription.";               
                for (Map.Entry<String, String> entry : erreurs.entrySet()) {
                    System.out.println(entry.getKey() + "/" + entry.getValue());
                }
            }
        } catch ( DAOException e ) {
            resultat = "�chec de l'inscription : une erreur impr�vue est survenue, merci de r�essayer dans quelques instants.";
            e.printStackTrace();
        }
	    
	    return personnel;
		
	}
	
	private void setErreur( String champ, String message ) {
	    erreurs.put( champ, message );
	}

	private void validationEmail( String email ) throws Exception {
	    if ( email != null ) {
	        if ( !email.matches( "([^.@]+)(\\.[^.@]+)*@([^.@]+\\.)+([^.@]+)" ) ) {
	            throw new Exception( "Merci de saisir une adresse mail valide." );
	        }
	    } else {
	        throw new Exception( "Merci de saisir une adresse mail." );
	    }
	}


	private void validationNom( String nom ) throws Exception {
	    if ( nom != null && nom.length() < 3 ) {
	        throw new Exception( "Le nom d'utilisateur doit contenir au moins 3 caract�res." );
	    }
	}
	
	private void validationPrenom( String prenom ) throws Exception {
	    if ( prenom != null && prenom.length() < 3 ) {
	        throw new Exception( "Le prenom doit contenir au moins 3 caract�res." );
	    }
	}
	
	private static String getValeurChamp( HttpServletRequest request, String nomChamp ) {
	    String valeur = request.getParameter( nomChamp );
	    if ( valeur == null || valeur.trim().length() == 0 ) {
	        return null;
	    } else {
	        return valeur.trim();
	    }
	}    
}
