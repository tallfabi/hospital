package form;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import dao.DAOException;
import dao.PersonnelDao;
import model.Patient;
import model.Personnel;
import model.Specialite;
import model.Ticket;

public class TicketForm {
    private static final String CHAMP_NOM    = "nom";
    private static final String CHAMP_GS    = "gs";
    private static final String CHAMP_PRENOM    = "prenom";
    private static final String CHAMP_SEXE    = "sexe";
    private static final String CHAMP_TELEPHONE    = "telephone";
    private static final String CHAMP_NAISSANCE    = "date_naissance";
    private static final String CHAMP_SPECIALITE    = "specialite";
    private static final String CHAMP_ID_PATIENT    = "idPatient";
    
	private PersonnelDao personnelDao;
	
	public TicketForm(PersonnelDao personnelDao) {
		this.personnelDao = personnelDao;
	}
	
	
	public void addTicket(HttpServletRequest  request) {
	    String gs = getValeurChamp( request, CHAMP_GS );
	    String nom = getValeurChamp( request, CHAMP_NOM );
	    String prenom = getValeurChamp( request, CHAMP_PRENOM );
	    String sexe = getValeurChamp( request, CHAMP_SEXE );	
	    String naissance = getValeurChamp( request, CHAMP_NAISSANCE );	
	    String telephone = getValeurChamp( request, CHAMP_TELEPHONE );	
	    String specialiteId = getValeurChamp( request, CHAMP_SPECIALITE );
	    String idPatientChamp = getValeurChamp( request, CHAMP_ID_PATIENT );
	    
	    System.out.println("champ patient : "+ idPatientChamp);
	    
	    Ticket ticket = new Ticket();
	    
	    Specialite specialite = new Specialite();
	    
	    specialite.setId(Integer.parseInt(specialiteId));
	    
	    ticket.setSpecialite(specialite);
	    
        HttpSession session = request.getSession();
        
        Personnel caissier = (Personnel) session.getAttribute("caissier");	
        

	    ticket.setCaissier(caissier);
	    
	    try {

	        	
        	if(request.getParameter("new") != null) {
        		
        		System.out.println("i am here");
        		
        		Patient patient = new Patient();
        		int idPatient = -1;
        		
        	    patient.setNom( nom );
        	    
        	    Random rand = new Random();
        	    String uniqueID = String.format("%04d", rand.nextInt(10000));
        	    patient.setMatricule(uniqueID);
        	    
        	    DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        	    Date fd = new Date();
        	    
        	    
        		try {
        			Date parsedDate = formatter.parse(naissance);
        			String enddt= formatter.format(parsedDate);
        			fd = formatter.parse(enddt);
        		} catch (ParseException e1) {

        			e1.printStackTrace();
        		}
        	    java.sql.Date sqlDate = new java.sql.Date(fd.getTime());
        	    

        	    patient.setNaissance(sqlDate);
        	    
        	    patient.setSexe(sexe);
        	    patient.setTelephone(telephone);
        	    
        	    patient.setPrenom(prenom);
        	    patient.setGs(gs);
        	    
        	    idPatient = personnelDao.ajouterPatient(patient);
        	    patient.setId(idPatient);
        	    ticket.setPatient(patient);
        		personnelDao.vendreTicket(ticket);
        		
                        		
        	}else{
        		Patient patient = new Patient();
        		patient.setId(Integer.parseInt(idPatientChamp));
        		ticket.setPatient(patient);
        		
        		personnelDao.vendreTicket(ticket);
        	}

	    } catch ( DAOException e ) {
	        System.out.println("�chec de l'inscription : une erreur impr�vue est survenue, merci de r�essayer dans quelques instants.");
	        e.printStackTrace();
	    }
	}
	
	private static String getValeurChamp( HttpServletRequest request, String nomChamp ) {
	    String valeur = request.getParameter( nomChamp );
	    if ( valeur == null || valeur.trim().length() == 0 ) {
	        return null;
	    } else {
	        return valeur.trim();
	    }
	}  
}
