<%@ page language="java" contentType="text/html; charset=UTF-8"%>
    <%@ include file="layout/head.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Alexander Pierce</p>
          <!-- Status -->
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>

      <!-- search form (Optional) -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
              <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
              </button>
            </span>
        </div>
      </form>
      <!-- /.search form -->

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">HEADER</li>
        <!-- Optionally, you can add icons to the links -->
        <li class="active"><a href="#"><i class="fa fa-link"></i> <span>Link</span></a></li>
        <li><a href="#"><i class="fa fa-link"></i> <span>Another Link</span></a></li>
        <li class="treeview">
          <a href="#"><i class="btn btn-primary"></i> <span>Ajouter</span>
            <span class="pull-right-container">
              </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#">Link in level 2</a></li>
            <li><a href="#">Link in level 2</a></li>
          </ul>
        </li>
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Ajout ticket
        <small>Optional description</small>
      </h1>
      <ol class="breadcrumb">
          <a href="#" class="btn btn-primary ">
          <i class="fa fa-plus" ></i>
          </a>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">

      <div class="row" >
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Ajouter un ticket</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
										
			<div class="row" style="padding: 20px;">
				<ul class="nav nav-tabs" id="myTab" role="tablist">
				  <li class="nav-item active">
				    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#old" role="tab" aria-controls="home" aria-selected="true">Ancien patient</a>
				  </li>
				  <li class="nav-item">
				    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#new" role="tab" aria-controls="profile" aria-selected="false">Nouveau patient</a>
				  </li>
				</ul>
				<div class="tab-content" id="myTabContent">
				  <div class="tab-pane fade active in" id="old" role="tabpanel" aria-labelledby="old-tab">
					
					<div class="row">
					<form method="get" action="/hospital/caissier/ajout/ticket" >

					  
						<div class="col-sm-4">
						  <div class="form-group">
						      <label for="recherche">Recherche</label>
                              <input type="text" class="form-control" id="telPatient" name="telPatient" placeholder="Entrez le numéro de téléphone du patient">
						  </div>
					    </div>
					    <div class="col-sm-2 text-left">
					    <button style="margin-top: 25px;" type="submit" class="btn btn-primary">Valider</button>
					    </div>
					    </form>
					   </div>
					    
					    <hr />
					    
					    <form method="post" action="/hospital/caissier/ajout/ticket">
					    
	    				   <div class="row">
	    				   
		    				<div class="col-sm-4">
							  <div class="form-group">
							      <label for="inputState">Spécialité</label>
									<select name="specialite" class="form-control m-input" id="exampleSelect1" >
								        <option value="1">Cardiologie</option>
								        <option value="2">ophthalmology</option>
									</select>
							  </div>
						    </div>
						    
						   </div> 
						    
						   <div class="row">

						    <div class="col-sm-4">
								<div  class="form-group">
								    <input value="${ patient.getId() }" type="hidden" name="idPatient" class="form-control m-input" id="idPatient">
									<label for="exampleInputNom">Nom</label>
									<input value="${ patient.getNom() }" type="text" name="nom" disabled class="form-control m-input" id="exampleInputNom" aria-describedby="NomHelp" placeholder="Entrez votre nom">
								</div>
						    </div>
						    
						    <div class="col-sm-4">
								<div class="form-group">
									<label for="exampleInputPrenom">Prénom</label>
									<input type="text"value="${ patient.getPrenom() }" disabled  name="prenom" class="form-control m-input" id="exampleInputPrenom" aria-describedby="PrenomHelp" placeholder="Entrez votre prénom">
								</div>
						    </div>

                           </div>
                                       
                           <div class="row" >

						    <div class="col-sm-4">
								<div  class="form-group ">
									<label for="exampleInputNom">Sexe</label>
								<select name="sexe" class="form-control m-input" id="exampleSelect1" disabled>
										<option>MASCULIN</option>
										<option>FEMININ</option>														
									</select>
								</div>
						    </div>
						    
						    <div class="col-sm-4">
                                        <div class="form-group"  >
									<label for="exampleSelect1">Groupe Sanguin</label>
									<select name="gs" class="form-control m-input" id="exampleSelect1" disabled>

										<option>A</option>
										<option>APOSITIF</option>
										<option>ANEGATIF</option>
										<option>O</option>
										<option>OPOSITIF</option>
										<option>ONEGATIF</option>
										<option>B</option>
										<option>BPOSITIF</option>
										<option>BNEGATIF</option>
										<option>ABPOSITIF</option>
										<option>OBNEGATIF</option>
									</select>
								</div>
						    </div>

                          </div>
                                       
                        <div class="row" >

						    <div class="col-sm-4">
								<div class="form-group">
									<label for="exampleInputTel">Téléphone</label>
									<input value="${ patient.getTelephone() }" type="text" name="telephone" class="form-control m-input" id="exampleInputTel" aria-describedby="TelHelp" placeholder="Entrez votre Téléphone" disabled>
								</div>
						    </div>
							
							<div class="col-sm-4">
								<div class="form-group">
									<label for="exampleInputNaissance">Date de naissance</label>
									<input value="${ patient.getNaissance() }" autocomplete="off" id="date_naissance2" name="date_naissance" type="text" class="form-control m-input" aria-describedby="NaissanceHelp" disabled placeholder="Date de naissance">
								</div>
						    </div>    

                       </div>
                   		<div class="m-portlet__foot m-portlet__foot--fit">
							<div class="m-form__actions">
								<button type="submit" class="btn btn-primary">Valider</button>
								<button type="reset" class="btn btn-secondary">Annuler</button>
							</div>
						</div>
					</form>
                  </div>
				  <div class="tab-pane fade" id="new" role="tabpanel" aria-labelledby="new-tab">
				  
				  	   <form method="post" action="/hospital/caissier/ajout/ticket">
					   
		   					<div class="row">
							<div class="col-sm-4">
							  <div class="form-group">
							      <label for="inputState">Spécialité</label>
									<select name="specialite" class="form-control m-input" id="exampleSelect1" >
								        <option value="1">Cardiologie</option>
								        <option value="2">ophthalmology</option>
									</select>
							  </div>
						    </div> 
						    </div>
	    				   <div class="row">
						    <div class="col-sm-4">
								<div  class="form-group">
									<input value="new" type="hidden" name="new" class="form-control m-input">
									<label for="exampleInputNom">Nom</label>
									<input value="${ patient.getNom() }" type="text" name="nom" class="form-control m-input" id="exampleInputNom" aria-describedby="NomHelp" placeholder="Entrez votre nom">
								</div>
						    </div>
						    
						    <div class="col-sm-4">
								<div class="form-group">
									<label for="exampleInputPrenom">Prénom</label>
									<input type="text" value="${ patient.getPrenom() }"  name="prenom" class="form-control m-input" id="exampleInputPrenom" aria-describedby="PrenomHelp" placeholder="Entrez votre prénom">
								</div>
						    </div>

                           </div>
                                       
                           <div class="row" >

						    <div class="col-sm-4">
								<div  class="form-group ">
									<label for="exampleInputNom">Sexe</label>
								<select name="sexe" class="form-control m-input" id="exampleSelect1">
										<option>MASCULIN</option>
										<option>FEMININ</option>														
									</select>
								</div>
						    </div>
						    
						    <div class="col-sm-4">
                                        <div class="form-group"  >
									<label for="exampleSelect1">Groupe Sanguin</label>
									<select name="gs" class="form-control m-input" id="exampleSelect1">

										<option>A</option>
										<option>APOSITIF</option>
										<option>ANEGATIF</option>
										<option>O</option>
										<option>OPOSITIF</option>
										<option>ONEGATIF</option>
										<option>B</option>
										<option>BPOSITIF</option>
										<option>BNEGATIF</option>
										<option>ABPOSITIF</option>
										<option>OBNEGATIF</option>
									</select>
								</div>
						    </div>

                          </div>
                                       
                        <div class="row" >

						    <div class="col-sm-4">
								<div class="form-group">
									<label for="exampleInputTel">Téléphone</label>
									<input value="${ patient.getTelephone() }" type="text" name="telephone" class="form-control m-input" id="exampleInputTel" aria-describedby="TelHelp" placeholder="Entrez votre Téléphone">
								</div>
						    </div>
							
							<div class="col-sm-4">
								<div class="form-group">
									<label for="exampleInputNaissance">Date de naissance</label>
									<input autocomplete="off" id="date_naissance" name="date_naissance" type="text" class="form-control m-input" aria-describedby="NaissanceHelp" placeholder="Date de naissance">
								</div>
						    </div>    

                       </div>
						<div class="m-portlet__foot m-portlet__foot--fit">
							<div class="m-form__actions">
								<button type="submit" class="btn btn-primary">Valider</button>
								<button type="reset" class="btn btn-secondary">Annuler</button>
							</div>
						</div>
					</form>
				  </div>
				</div>
            </div>

         </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2016 <a href="#">Company</a>.</strong> All rights reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane active" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:;">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:;">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="pull-right-container">
                    <span class="label label-danger pull-right">70%</span>
                  </span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/dist/js/adminlte.min.js"></script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
     <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
     <script type="text/javascript"> 

 $( "#date_naissance" ).datepicker();
 $( "#date_naissance2" ).datepicker();

  
   
</script> 
</body>
</html>