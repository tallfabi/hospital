<%@ page language="java" contentType="text/html; charset=UTF-8"%>
    <%@ include file="layout/head.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Alexander Pierce</p>
          <!-- Status -->
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>

      <!-- search form (Optional) -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
              <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
              </button>
            </span>
        </div>
      </form>
      <!-- /.search form -->

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">HEADER</li>
        <!-- Optionally, you can add icons to the links -->
        <li class="active"><a href="#"><i class="fa fa-link"></i> <span>Link</span></a></li>
        <li><a href="#"><i class="fa fa-link"></i> <span>Another Link</span></a></li>
        <li class="treeview">
          <a href="#"><i class="btn btn-primary"></i> <span>Ajouter</span>
            <span class="pull-right-container">
              </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#">Link in level 2</a></li>
            <li><a href="#">Link in level 2</a></li>
          </ul>
        </li>
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Page Header
        <small>Optional description</small>
      </h1>
      <ol class="breadcrumb">
          <a href="#" class="btn btn-primary ">
          <i class="fa fa-plus" ></i>
          </a>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">

      <div class="row">
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Ajout Personnel</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
						<div class="m-portlet__body">
										
								<div class="row">
                                   <form method="post" action="/hospital/modifier/personnel?idPersonnel=${ personnel.getId() }">
									    <div class="col-sm-6">
											<div  class="form-group m-form__group">
												<label for="exampleInputNom">Nom</label>
												<input value="${ personnel.getNom() }" type="text" name="nom" class="form-control m-input" id="exampleInputNom" aria-describedby="NomHelp" placeholder="Entrez votre nom">
											</div>
									    </div>
									    
									    <div class="col-sm-6">
											<div class="form-group m-form__group">
												<label for="exampleInputPrenom">Prénom</label>
												<input value="${ personnel.getPrenom() }"  type="text" name="prenom" class="form-control m-input" id="exampleInputPrenom" aria-describedby="PrenomHelp" placeholder="Entrez votre prénom">
											</div>
									    </div>

                                          </div>
                                          
                                         <div class="row" style="margin-top: 20px;">

									    <div class="col-sm-6">
											<div  class="form-group m-form__group">
												<label for="exampleInputTel">Téléphone</label>
												<input value="${ personnel.getTelephone() }"  type="text" name="telephone" class="form-control m-input" id="exampleInputTel" aria-describedby="TelHelp" placeholder="Entrez votre Téléphone">
											</div>
									    </div>
									    
									    <div class="col-sm-6">
											<div  class="form-group m-form__group">
												<label for="exampleInputEmail">E-mail</label>
												<input value="${ personnel.getEmail() }"  type="email" name=email class="form-control m-input" id="exampleInputEmail" aria-describedby="EmailHelp" placeholder="Entrez votre e-mail">
											</div>
									    </div>

                                          </div>
                                          
                                         <div class="row" style="margin-top: 20px;">

									    <div class="col-sm-6">
											<div  class="form-group m-form__group">
												<label for="exampleInputNaissance">Date de naissance</label>
												<input value="${ personnel.getNaissance() }"  name="naissance" type="date" class="form-control m-input" id="exampleInputNaissance" aria-describedby="NaissanceHelp" placeholder="Date de naissance" required>
											</div>
									    </div>
									    
									    <div class="col-sm-6">
											<divclass="form-group m-form__group">
												<label for="exampleInputDomaine">Domaine</label>
												<input value="${ personnel.getDomaine() }"  name="domaine" type="text" class="form-control m-input" id="exampleInputDomaine" aria-describedby="DomaineHelp" placeholder="Domaine">
											</div>
									    </div>

                                          </div>
                                          
                                          <div class="row" style="margin-top: 20px;">

									    <div class="col-sm-6">
                                           <div class="form-group m-form__group" style="padding-left: 0px;" >
												<label for="exampleSelect1">Type Personnel</label>
												<select name="type" class="form-control m-input" id="exampleSelect1">
													<option>MEDECIN</option>
													<option>INFIRMIER</option>
													<option>CAISSIER</option>
													<option>SAGEFEMME</option>
													<option>RH</option>
												</select>
											</div>
									    </div>
									   

                                          </div>
										
									</div>
									<div class="m-portlet__foot m-portlet__foot--fit">
										<div class="m-form__actions">
											<button type="submit" class="btn btn-primary">Valider</button>
											<button type="reset" class="btn btn-secondary">Annuler</button>
										</div>
									</div>
								</form>
          </div>
        </div>
         </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2016 <a href="#">Company</a>.</strong> All rights reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane active" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:;">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:;">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="pull-right-container">
                    <span class="label label-danger pull-right">70%</span>
                  </span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/dist/js/adminlte.min.js"></script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
</body>
</html>